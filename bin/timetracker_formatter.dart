import 'dart:convert';
import 'dart:io';

const JsonDecoder decoder = JsonDecoder();

class Timetracker {
  int? total;
  List<TimeEntry>? sessions;

  Timetracker.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    var arr = json['sessions'].map((e) => TimeEntry.fromJson(e)).toList();
    sessions = arr.cast<TimeEntry>();
  }
}

class TimeEntry {
  DateTime? begin;
  DateTime? end;
  int? duration;

  TimeEntry.fromJson(Map<String, dynamic> json) {
    begin = DateTime.parse(json['begin']);
    end = DateTime.parse(json['end']);
    duration = json['duration'];
  }
}

void main(List<String> arguments) {
  String path = arguments[0];
  String html = topofHTML();

  var jsonString = File(path).readAsStringSync();
  var json = decoder.convert(jsonString);
  Timetracker tracker = Timetracker.fromJson(json);

  print("Session\t\tDate\t\tBegin\tEnd\tDuration");
  for (int i = 0; i < tracker.sessions!.length; i++) {
    String all = "";
    all += "Session ${i + 1}: \t";
    all +=
        ("${tracker.sessions![i].begin?.year}-${monthfrInt(tracker.sessions![i].begin!.month)}-${tracker.sessions![i].begin!.day}\t");
    all += ("${tracker.sessions![i].begin?.hour}:");
    all += (tracker.sessions![i].begin!.minute < 10) ? "0" : "";
    all += ("${tracker.sessions![i].begin?.minute}\t");
    all += ("${tracker.sessions![i].end?.hour}:");
    all += (tracker.sessions![i].end!.minute < 10) ? "0" : "";
    all += ("${tracker.sessions![i].end?.minute}\t");

    int millis = tracker.sessions![i].end!.millisecondsSinceEpoch -
        tracker.sessions![i].begin!.millisecondsSinceEpoch;
    int seconds = (millis / 1000).round();
    int roundMinutes = ((seconds - (seconds % 60)) / 60).round();
    seconds = seconds % 60;
    int roundHours = ((roundMinutes - (roundMinutes % 60)) / 60).round();
    roundMinutes = roundMinutes % 60;
    all += ("${roundHours}h ${roundMinutes}m ${seconds}s");
    print(all);
    String dag =
        "${tracker.sessions![i].begin?.year}-${monthfrInt(tracker.sessions![i].begin!.month)}-${tracker.sessions![i].begin!.day}";
    String begin = ("${tracker.sessions![i].begin?.hour}:");
    begin += (tracker.sessions![i].begin!.minute < 10) ? "0" : "";
    begin += ("${tracker.sessions![i].begin?.minute}");
    String end = ("${tracker.sessions![i].end?.hour}:");
    end += (tracker.sessions![i].end!.minute < 10) ? "0" : "";
    end += ("${tracker.sessions![i].end?.minute}\t");
    String werktijd = ("${roundHours}h ${roundMinutes}m ${seconds}s");
    String tarief = "€ 18,00";
    String bedrag =
        "€ ${((roundHours * 18) + (roundMinutes * 0.3)).toStringAsFixed(2)}";
    html += mid(dag, begin, end, werktijd, tarief, bedrag);
  }

  html += bottomofHTML();
  //Open file and stuff
  var indexFile = File("index.html");
  var sink = indexFile.openWrite();
  sink.write(html);
  sink.close();
  print("done");
  return;
}

String monthfrInt(int month) {
  switch (month) {
    case 1:
      return "Jan";
    case 2:
      return "Feb";
    case 3:
      return "Mar";
    case 4:
      return "Apr";
    case 5:
      return "May";
    case 6:
      return "Jun";
    case 7:
      return "Jul";
    case 8:
      return "Aug";
    case 9:
      return "Sep";
    case 10:
      return "Oct";
    case 11:
      return "Nov";
    case 12:
      return "Dec";
    default:
      return "Error";
  }
}

String topofHTML() {
  return """
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Invoice</title>

		<style>
			.invoice-box {
				max-width: 800px;
				margin: auto;
				padding: 30px;
				border: 1px solid #eee;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
				font-size: 16px;
				line-height: 24px;
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				color: #555;
			}

			.invoice-box table {
				width: 100%;
				line-height: inherit;
				text-align: left;
			}

			.invoice-box table td {
				padding: 5px;
				vertical-align: top;
			}

			.invoice-box table tr.top table td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.top table td.title {
				font-size: 45px;
				line-height: 45px;
				color: #333;
			}

			.invoice-box table tr.information table td {
				padding-bottom: 40px;
			}

			.invoice-box table tr.heading td {
				background: #eee;
				border-bottom: 1px solid #ddd;
				font-weight: bold;
			}

			.invoice-box table tr.details td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.item td {
				border-bottom: 1px solid #eee;
			}

			.invoice-box table tr.item.last td {
				border-bottom: none;
			}

			.invoice-box table tr.total {
				border-top: 2px solid #eee;
                font-weight: bold;
			}

			@media only screen and (max-width: 600px) {
				.invoice-box table tr.top table td {
					width: 100%;
					display: block;
					text-align: center;
				}

				.invoice-box table tr.information table td {
					width: 100%;
					display: block;
					text-align: center;
				}
			}

			/** RTL **/
			.invoice-box.rtl {
				direction: rtl;
				font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
			}

			.invoice-box.rtl table {
				text-align: right;
			}

			.invoice-box.rtl table tr td:nth-child(2) {
				text-align: left;
			}
		</style>
	</head>

	<body>
		<div class="invoice-box">
			<table cellpadding="0" cellspacing="0">
				<tr class="top">
					<td colspan="6">
						<table>
							<tr>
								<td class="title">
									<img src="https://lh3.googleusercontent.com/u/0/drive-viewer/AAOQEOTNcdoBI1c16RPzWDTHEECfsn5wKUF3-m22aE8AGU45amgUcG3GWIj-WD-7cLKPbo_cuhlquiIxQFEkIsUrxOUdVipesw=w960-h431" style="width: 100%; max-width: 150px" />
								</td>
                                <td>Rolf Moerland <br />
                                    IOV Schef <br />
                                    Mobile Development
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
								<td>
									Invoice #: 001<br />
									Created: 2023-02-18<br />
									Due: 2023-04-18
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr class="information">
					<td colspan="6">
						<table>
							<tr>
								<td>
									Rolf Moerland<br />
									5091 SN<br />
									Jachtlaan 2, Middelbeers
								</td>

								<td>
									Learn IT<br />
									NL003032809B10<br />
									moerland.rolf@gmail.com
								</td>
							</tr>
						</table>
					</td>
				</tr>

                <p style="color:#aaa">Dit bestand is automatisch gegenereerd en handmatig nagekeken. Zie meegeleverde .timetracker voor uitgebreiderde informatie in combinatie met git commits.</p>
                
                

				<tr class="heading">
					<td>Dag</td>
					<td>Begin</td>
					<td>Eind</td>
                    <td>Werktijd</td>
                    <td>Tarief</td>
					<td>Prijs</td>
				</tr>
  """;
}

String mid(String dag, String begin, String eind, String werktijd,
    String tarief, String prijs) {
  return """
                  
				<tr class="item">
                    <td>$dag</td>
                    <td>$begin</td>
                    <td>$eind</td>
                    <td>$werktijd</td>
                    <td>$tarief</td>
                    <td>$prijs</td>
				</tr>
""";
}

String bottomofHTML() {
  return """
				<tr class="total">
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>Totaal:</td>
					<td>385.00</td>
				</tr>
			</table>
		</div>
	</body>
</html>
""";
}
